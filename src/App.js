

import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, FormGroup, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      check1: false,
      check2: false,
      check3: false,
      text1Val: '',
      text2Val: '',
      text3Val: '',
      showallData: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal,
      showallData: this.state.modal,
    });
  }
  checkHandle = (event) => {
    this.setState({
      [event.target.name]: event.target.checked,
    });
  }
  handleInput = (event) => {
    this.setState({
      [`${event.target.name}Val`]: event.target.value,
    });
  }

  handleSubmit = () => {
    this.toggle();
    this.setState({
      showallData: true,
    })
  }
  render() {
    const { check1, check2, check3, showallData } = this.state;
    console.log(this.state)
    return (
      <div>
        <Button color="danger" onClick={this.toggle}>Open Form</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
          <ModalBody>
          <FormGroup check>
          <Label check>
            <Input type="checkbox" name="check1" onChange={this.checkHandle} />{' '}
            Check me out
    {check1 && <Input type="text" name="text1" onChange={this.handleInput} value={this.state.text1Val} />}
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input type="checkbox" name="check2" onChange={this.checkHandle} />{' '}
            Check me out
    {check2 && <Input type="text" name="text2" onChange={this.handleInput} value={this.state.text2Val} />}
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input type="checkbox" name="check3" onChange={this.checkHandle} />{' '}
            Check me out
    {check3 && <Input type="text" name="text3" onChange={this.handleInput} value={this.state.text3Val} />}
          </Label>
        </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.handleSubmit}>Do Something</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
        {showallData && 
        <Card>
        <CardBody>
          <CardTitle>Submitted Data</CardTitle>
          <CardSubtitle>First input value: </CardSubtitle>
          <CardText>{this.state.text1Val}</CardText>
          <CardSubtitle>Second input value: </CardSubtitle>
          <CardText>{this.state.text2Val}</CardText>
          <CardSubtitle>Third input value: </CardSubtitle>
          <CardText>{this.state.text3Val}</CardText>
        </CardBody>
      </Card>}
      </div>
    );
  }
}


export default App;
